package com.tom_novak.android.astrumqtest.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.tom_novak.android.astrumqtest.R;
import com.tom_novak.android.astrumqtest.TestApplication;
import com.tom_novak.android.astrumqtest.dagger.AppComponent;
import com.tom_novak.android.astrumqtest.dagger.DaggerMainComponent;
import com.tom_novak.android.astrumqtest.dagger.MainModule;
import com.tom_novak.android.astrumqtest.domain.Message;
import com.tom_novak.android.astrumqtest.presenter.MainPresenter;
import com.tom_novak.android.astrumqtest.view.MainView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, MainView {

    public static final int REQ_CODE_LOGIN = 9000;

    @Inject
    TestApplication app;
    @Inject
    MainPresenter presenter;

    @BindView(R.id.messages_list)
    RecyclerView messagesView;
    @BindView(R.id.message_edit)
    EditText messageEditView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        presenter.onCreate();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.loadNextMessages();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQ_CODE_LOGIN) {
            if (resultCode == RESULT_CANCELED) {
                finish();
            }
        }
    }

    @Override
    public String getData() {
        return messageEditView.getText().toString();
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void showLoginScreen() {
        startActivityForResult(new Intent(this, LoginActivity.class), REQ_CODE_LOGIN);
    }

    @Override
    public void showMessages(List<Message> messages) {
        // TODO
    }

    @Override
    protected void setupComponent(AppComponent appComponent) {
        DaggerMainComponent.builder()
                .mainModule(new MainModule(this))
                .appComponent(appComponent)
                .build()
                .inject(this);
    }

    @Override
    protected int getContentViewRes() {
        return R.layout.activity_main;
    }

    @OnClick(R.id.message_send)
    public void onMessageSend() {
        presenter.doSendMessage();
    }
}
