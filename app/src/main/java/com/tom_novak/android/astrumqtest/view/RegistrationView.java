package com.tom_novak.android.astrumqtest.view;

import com.tom_novak.android.astrumqtest.domain.AuthData;
import com.tom_novak.android.astrumqtest.domain.AuthResponse;
import com.tom_novak.android.astrumqtest.domain.Salt;
import com.tom_novak.android.astrumqtest.domain.UserDetail;

/**
 * Created by tom on 11.11.16.
 */

public interface RegistrationView extends IView, IFormView<UserDetail> {

    void processSuccessRegistration(AuthResponse response);
}
