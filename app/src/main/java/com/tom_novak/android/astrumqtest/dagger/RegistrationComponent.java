package com.tom_novak.android.astrumqtest.dagger;

import com.tom_novak.android.astrumqtest.ActivityScope;
import com.tom_novak.android.astrumqtest.activity.LoginActivity;
import com.tom_novak.android.astrumqtest.activity.RegistrationActivity;

import dagger.Component;

/**
 * Created by tom on 13.11.16.
 */

@ActivityScope
@Component(
        modules = RegistrationModule.class,
        dependencies = AppComponent.class
)
public interface RegistrationComponent {

    void inject(RegistrationActivity activity);
}
