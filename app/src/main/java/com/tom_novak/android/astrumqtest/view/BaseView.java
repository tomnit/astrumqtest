package com.tom_novak.android.astrumqtest.view;

/**
 * Created by tom on 13.11.16.
 */

public interface BaseView extends IView {

    int EMPTY = 0;
    int PROGRESS = 1;
    int CONTENT = 2;

    void showViewState(int viewState);
}
