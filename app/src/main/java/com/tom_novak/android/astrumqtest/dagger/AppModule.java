package com.tom_novak.android.astrumqtest.dagger;

import android.content.Context;

import com.tom_novak.android.astrumqtest.IPreferencesRepository;
import com.tom_novak.android.astrumqtest.PreferencesRepository;
import com.tom_novak.android.astrumqtest.TestApplication;
import com.tom_novak.android.astrumqtest.api.ApiInterface;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by tom on 11.11.16.
 */

@Module
public class AppModule {

    private TestApplication app;

    public AppModule(TestApplication app) {
        this.app = app;
    }

    @Provides
    @Singleton
    public TestApplication providesAppContext() {
        return this.app;
    }

    @Provides
    @Singleton
    public IPreferencesRepository providePreferencesRepository(TestApplication application) {
        return new PreferencesRepository(application);
    }
}
