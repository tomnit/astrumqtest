package com.tom_novak.android.astrumqtest.dagger;

import com.tom_novak.android.astrumqtest.ActivityScope;
import com.tom_novak.android.astrumqtest.TestApplication;
import com.tom_novak.android.astrumqtest.activity.MainActivity;

import dagger.Component;

/**
 * Created by tom on 13.11.16.
 */

@ActivityScope
@Component(
        modules = MainModule.class,
        dependencies = AppComponent.class
)
public interface MainComponent {

    void inject(MainActivity activity);
}
