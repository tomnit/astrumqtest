package com.tom_novak.android.astrumqtest.rx;


import rx.functions.Action1;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
import rx.subjects.Subject;

/**
 * Created by tom on 10.9.16.
 */
public class RxBus {

    private final Subject<Object, Object> rxSubject = new SerializedSubject<>(PublishSubject.create());

    public void fire(Object event) {
        rxSubject.onNext(event);
    }

    public void subscribe(Action1 action) {
        rxSubject.subscribe(action);
    }
}
