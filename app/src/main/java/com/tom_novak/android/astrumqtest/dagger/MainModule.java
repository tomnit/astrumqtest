package com.tom_novak.android.astrumqtest.dagger;

import com.tom_novak.android.astrumqtest.IPreferencesRepository;
import com.tom_novak.android.astrumqtest.api.ApiInterface;
import com.tom_novak.android.astrumqtest.api.HeadersInterceptor;
import com.tom_novak.android.astrumqtest.presenter.MainPresenter;
import com.tom_novak.android.astrumqtest.view.MainView;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by tom on 11.11.16.
 */

@Module
public class MainModule {

    private MainView mainView;

    public MainModule(MainView view) {
        this.mainView = view;
    }

    @Provides
    public MainView provideView() {
        return mainView;
    }

    @Provides
    public MainPresenter providePresenter(MainView view, Retrofit retrofit,
                                          ApiInterface api, IPreferencesRepository preferences,
                                          HeadersInterceptor interceptor) {
        return new MainPresenter(view, retrofit, api, preferences, interceptor);
    }
}
