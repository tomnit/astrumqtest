package com.tom_novak.android.astrumqtest.dagger;

import com.tom_novak.android.astrumqtest.api.ApiInterface;
import com.tom_novak.android.astrumqtest.presenter.LoginPresenter;
import com.tom_novak.android.astrumqtest.view.LoginView;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by tom on 11.11.16.
 */

@Module
public class LoginModule {

    private LoginView view;

    public LoginModule(LoginView view) {
        this.view = view;
    }

    @Provides
    public LoginView provideView() {
        return view;
    }

    @Provides
    public LoginPresenter providePresenter(LoginView view, Retrofit retrofit, ApiInterface api) {
        return new LoginPresenter(view, retrofit, api);
    }

}
