package com.tom_novak.android.astrumqtest.api;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by tom on 16.11.16.
 */

public class HeadersInterceptor implements Interceptor {

    private String authToken;

    @Override
    public Response intercept(Chain chain) throws IOException {
        if(authToken == null) {
            return chain.proceed(chain.request());
        } else {
            Request original = chain.request();
            Request.Builder reqBuilder = original.newBuilder()
                    .header("Authorization", authToken);

            Request request = reqBuilder.build();
            return chain.proceed(request);
        }
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getAuthToken() {
        return authToken;
    }
}
