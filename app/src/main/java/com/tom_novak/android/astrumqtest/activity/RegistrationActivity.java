package com.tom_novak.android.astrumqtest.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;

import com.tom_novak.android.astrumqtest.R;
import com.tom_novak.android.astrumqtest.TestApplication;
import com.tom_novak.android.astrumqtest.dagger.AppComponent;
import com.tom_novak.android.astrumqtest.dagger.DaggerRegistrationComponent;
import com.tom_novak.android.astrumqtest.dagger.RegistrationModule;
import com.tom_novak.android.astrumqtest.domain.AuthResponse;
import com.tom_novak.android.astrumqtest.domain.UserDetail;
import com.tom_novak.android.astrumqtest.presenter.RegistrationPresenter;
import com.tom_novak.android.astrumqtest.view.RegistrationView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegistrationActivity extends BaseActivity implements RegistrationView {

    @Inject
    TestApplication app;
    @Inject
    RegistrationPresenter presenter;

    @BindView(R.id.name)
    EditText nameView;
    @BindView(R.id.surname)
    EditText surnameView;
    @BindView(R.id.email)
    EditText emailView;
    @BindView(R.id.password)
    EditText passwordView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void setupComponent(AppComponent appComponent) {
        DaggerRegistrationComponent.builder()
                .registrationModule(new RegistrationModule(this))
                .appComponent(appComponent)
                .build()
                .inject(this);
    }

    @Override
    protected int getContentViewRes() {
        return R.layout.activity_registration;
    }

    @Override
    public UserDetail getData() {
        UserDetail userDetail = new UserDetail();

        userDetail.setName(nameView.getText().toString());
        userDetail.setSurname(surnameView.getText().toString());
        userDetail.setEmail(emailView.getText().toString());
        userDetail.setPassword(passwordView.getText().toString());

        return userDetail;
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void processSuccessRegistration(AuthResponse response) {
        setResult(RESULT_OK);
        finish();
    }

    @OnClick(R.id.sign_up_btn)
    public void submit() {
        presenter.doRegister();
    }
}
