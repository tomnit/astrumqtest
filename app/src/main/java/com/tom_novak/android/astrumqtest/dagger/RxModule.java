package com.tom_novak.android.astrumqtest.dagger;

import com.tom_novak.android.astrumqtest.rx.RxBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by tom on 10.9.16.
 */
@Module
public class RxModule {

    @Provides
    @Singleton
    public RxBus provideRxBus() {
        return new RxBus();
    }
}
