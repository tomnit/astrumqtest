package com.tom_novak.android.astrumqtest.api;

import com.tom_novak.android.astrumqtest.domain.AuthData;
import com.tom_novak.android.astrumqtest.domain.AuthResponse;
import com.tom_novak.android.astrumqtest.domain.Message;
import com.tom_novak.android.astrumqtest.domain.MessageContainer;
import com.tom_novak.android.astrumqtest.domain.Messages;
import com.tom_novak.android.astrumqtest.domain.Salt;
import com.tom_novak.android.astrumqtest.domain.UserDetail;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * Created by tom on 11.11.16.
 */

public interface ApiInterface {

    public static final String BASE_URL = "http://production.astrumq.com/entry_test/www/api/";

    @Headers({
            "Content-type: application/json"
    })

    @POST("user/registration")
    Observable<AuthResponse> register(@Body UserDetail user);

    @GET("login/salt")
    Observable<Salt> getSalt(@Query("email") String email);

    @POST("login")
    Observable<AuthResponse> login(@Body AuthData data);

    @GET("messages")
    Observable<Messages> getMessages(@Query("limitFilter") int limitFilter,
                                     @Query("offsetFilter") int offsetFilter);

    @POST("messages")
    Observable<MessageContainer> addMessage(@Body Message message);

    @GET("user/{id}")
    Observable<ResponseBody> getUserDetail(@Path("id") int userId);

    @GET("user/{id}/image")
    Observable<ResponseBody> getUserImage(@Path("id") int userId);
}
