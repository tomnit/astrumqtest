package com.tom_novak.android.astrumqtest.view;

import com.tom_novak.android.astrumqtest.domain.ApiError;

/**
 * Created by tom on 14.11.16.
 */

public interface IView {

    void showProgress(boolean progress);

    void showError(ApiError apiError);
}
