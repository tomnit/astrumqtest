package com.tom_novak.android.astrumqtest.view;

import com.tom_novak.android.astrumqtest.domain.AuthData;
import com.tom_novak.android.astrumqtest.domain.AuthResponse;

/**
 * Created by tom on 11.11.16.
 */

public interface LoginView extends IView, IFormView<AuthData> {

    void processAuthResponse(AuthResponse response);
}
