package com.tom_novak.android.astrumqtest.presenter;

import com.tom_novak.android.astrumqtest.ErrorHandlingSubscriber;
import com.tom_novak.android.astrumqtest.api.ApiInterface;
import com.tom_novak.android.astrumqtest.domain.AuthData;
import com.tom_novak.android.astrumqtest.domain.AuthResponse;
import com.tom_novak.android.astrumqtest.domain.ApiError;
import com.tom_novak.android.astrumqtest.domain.Salt;
import com.tom_novak.android.astrumqtest.utils.StringUtils;
import com.tom_novak.android.astrumqtest.view.LoginView;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import static rx.Completable.merge;

/**
 * Created by tom on 11.11.16.
 */

public class LoginPresenter {

    private final Retrofit retrofit;
    private final ApiInterface api;
    private final LoginView view;

    private Subscription subscription;

    @Inject
    public LoginPresenter(LoginView view, Retrofit retrofit, ApiInterface api) {
        this.view = view;
        this.retrofit = retrofit;
        this.api = api;
    }

    public void doLogin() {
        if(!view.isValid()) {
            return;
        }

        AuthData authData = view.getData();

        Observable<Salt> saltObservable = api.getSalt(authData.getEmail());
        subscription = saltObservable.observeOn(AndroidSchedulers.mainThread())
                .subscribe(new GetSaltSubscriber(retrofit, authData));
    }

    class GetSaltSubscriber extends ErrorHandlingSubscriber<Salt> {

        private AuthData authData;

        public GetSaltSubscriber(Retrofit retrofit, AuthData authData) {
            super(retrofit);
            this.authData = authData;
        }

        @Override
        protected void showError(ApiError apiError) {
            view.showError(apiError);
        }

        @Override
        public void onNext(Salt salt) {
            authData.setPassword(StringUtils.getSHA256(authData.getPassword(), salt.getSalt()));

            Observable<AuthResponse> authObservable = api.login(authData);
            subscription = authObservable.observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new LoginSubscriber(retrofit));
        }
    }

    class LoginSubscriber extends ErrorHandlingSubscriber<AuthResponse> {

        public LoginSubscriber(Retrofit retrofit) {
            super(retrofit);
        }

        @Override
        protected void showError(ApiError apiError) {
            view.showError(apiError);
        }

        @Override
        public void onNext(AuthResponse authResponse) {
            // TODO save token and finish activity
        }
    }

}
