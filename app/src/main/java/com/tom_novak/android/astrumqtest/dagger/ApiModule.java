package com.tom_novak.android.astrumqtest.dagger;

import com.tom_novak.android.astrumqtest.api.ApiInterface;
import com.tom_novak.android.astrumqtest.api.HeadersInterceptor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;
import rx.schedulers.Schedulers;

/**
 * Created by tom on 10.9.16.
 */
@Module
public class ApiModule {

    private Retrofit retrofit;
    private HeadersInterceptor headersInterceptor;

    public ApiModule() {
        RxJavaCallAdapterFactory rxAdapter
                = RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io());

        headersInterceptor = new HeadersInterceptor();

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(headersInterceptor)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(ApiInterface.BASE_URL)
                .client(client)
                .addConverterFactory(JacksonConverterFactory.create())
                .addCallAdapterFactory(rxAdapter)
                .build();
    }

    @Provides
    @Singleton
    public Retrofit provideRetrofit() {
        return retrofit;
    }

    @Provides
    @Singleton
    public ApiInterface provideApiService() {
        return retrofit.create(ApiInterface.class);
    }

    @Provides
    @Singleton
    public HeadersInterceptor provideHeadersInterceptor() {
        return headersInterceptor;
    }
}
