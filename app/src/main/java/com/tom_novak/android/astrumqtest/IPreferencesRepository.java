package com.tom_novak.android.astrumqtest;

import com.tom_novak.android.astrumqtest.domain.AuthResponse;

/**
 * Created by tom on 16.11.16.
 */

public interface IPreferencesRepository {

    void saveAuthData(AuthResponse data);

    AuthResponse getAuthData();

    void purgeAuthData();
}
