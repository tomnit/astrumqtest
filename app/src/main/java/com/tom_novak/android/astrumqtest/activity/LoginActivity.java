package com.tom_novak.android.astrumqtest.activity;

import android.content.Intent;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import java.util.HashMap;
import java.util.Map;

import com.tom_novak.android.astrumqtest.R;
import com.tom_novak.android.astrumqtest.TestApplication;
import com.tom_novak.android.astrumqtest.dagger.AppComponent;
import com.tom_novak.android.astrumqtest.dagger.DaggerLoginComponent;
import com.tom_novak.android.astrumqtest.dagger.LoginModule;
import com.tom_novak.android.astrumqtest.domain.AuthData;
import com.tom_novak.android.astrumqtest.domain.AuthResponse;
import com.tom_novak.android.astrumqtest.presenter.LoginPresenter;
import com.tom_novak.android.astrumqtest.view.LoginView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends BaseActivity implements LoginView {

    public static final Map<String, String> params = new HashMap<>();

    static {
        //params.put("email", "admin@astrumq.com");
        params.put("password", "administrator");
    }

    public static final int REQ_CODE_REGISTER = 9000;

    @Inject
    TestApplication app;
    @Inject
    LoginPresenter presenter;

    @BindView(R.id.email)
    EditText emailView;
    @BindView(R.id.password)
    EditText passwordView;
    @BindView(R.id.check_sign_in_perm)
    CheckBox permCheck;

    @BindView(R.id.sign_in_btn)
    Button signInButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK) {
            finish();
        }
    }

    @Override
    protected void setupComponent(AppComponent appComponent) {
        DaggerLoginComponent.builder()
                .loginModule(new LoginModule(this))
                .appComponent(appComponent)
                .build()
                .inject(this);
    }

    @Override
    protected int getContentViewRes() {
        return R.layout.activity_login;
    }

    @Override
    public void processAuthResponse(AuthResponse response) {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public AuthData getData() {
        AuthData authData = new AuthData();

        authData.setEmail(emailView.getText().toString());
        authData.setPassword(passwordView.getText().toString());

        return authData;
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @OnClick(R.id.sign_in_btn)
    public void onLoginClick(View view) {
        presenter.doLogin();
    }

    @OnClick(R.id.sign_up_btn)
    public void onRegisterClick(View view) {
        Intent intent = new Intent(this, RegistrationActivity.class);
        startActivityForResult(intent, REQ_CODE_REGISTER);
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }
}

