package com.tom_novak.android.astrumqtest.dagger;

import com.tom_novak.android.astrumqtest.IPreferencesRepository;
import com.tom_novak.android.astrumqtest.activity.MainActivity;
import com.tom_novak.android.astrumqtest.TestApplication;
import com.tom_novak.android.astrumqtest.activity.BaseActivity;
import com.tom_novak.android.astrumqtest.api.ApiInterface;
import com.tom_novak.android.astrumqtest.api.HeadersInterceptor;
import com.tom_novak.android.astrumqtest.view.MainView;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

/**
 * Created by tom on 10.9.16.
 */
@Singleton
@Component(
        modules = {
                AppModule.class,
                ApiModule.class
        }
)
public interface AppComponent {

    void inject(TestApplication application);

    TestApplication getTestApplication();

    ApiInterface getApiInterface();

    Retrofit getRetrofit();

    IPreferencesRepository getPreferences();

    HeadersInterceptor getHeadersInterceptor();
}
