package com.tom_novak.android.astrumqtest.dagger;

import com.tom_novak.android.astrumqtest.IPreferencesRepository;
import com.tom_novak.android.astrumqtest.api.ApiInterface;
import com.tom_novak.android.astrumqtest.presenter.RegistrationPresenter;
import com.tom_novak.android.astrumqtest.view.RegistrationView;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by tom on 11.11.16.
 */

@Module
public class RegistrationModule {

    private RegistrationView view;

    public RegistrationModule(RegistrationView view) {
        this.view = view;
    }

    @Provides
    public RegistrationView provideView() {
        return view;
    }

    @Provides
    public RegistrationPresenter providePresenter(
            RegistrationView view, Retrofit retrofit, ApiInterface api, IPreferencesRepository prefs) {
        return new RegistrationPresenter(view, retrofit, api, prefs);
    }
}
