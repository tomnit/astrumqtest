package com.tom_novak.android.astrumqtest.view;

/**
 * Created by tom on 14.11.16.
 */

public interface IFormView<T> {

    T getData();

    //void showData(T data);

    boolean isValid();
}
