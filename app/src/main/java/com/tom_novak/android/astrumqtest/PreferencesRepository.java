package com.tom_novak.android.astrumqtest;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.tom_novak.android.astrumqtest.domain.AuthResponse;

/**
 * Created by tom on 16.11.16.
 */

public class PreferencesRepository implements IPreferencesRepository {

    public static final String KEY_USER_ID = "userId";
    public static final String KEY_TOKEN = "token";

    private final SharedPreferences prefs;

    public PreferencesRepository(TestApplication application) {
        prefs = PreferenceManager.getDefaultSharedPreferences(application);
    }

    @Override
    public void saveAuthData(AuthResponse data) {
        SharedPreferences.Editor editor = prefs.edit();

        editor.putInt(KEY_USER_ID, data.getId());
        editor.putString(KEY_TOKEN, data.getToken());

        editor.commit();
    }

    @Override
    public AuthResponse getAuthData() {
        AuthResponse data = new AuthResponse();

        data.setId(prefs.getInt(KEY_USER_ID, -1));
        data.setToken(prefs.getString(KEY_TOKEN, "none"));

        if(data.getId() == -1 || data.getToken().equals("none")) {
            return null;
        }

        return data;
    }

    @Override
    public void purgeAuthData() {
        SharedPreferences.Editor editor = prefs.edit();

        editor.remove(KEY_USER_ID);
        editor.remove(KEY_TOKEN);

        editor.commit();
    }
}
