package com.tom_novak.android.astrumqtest.view;

import com.tom_novak.android.astrumqtest.domain.AuthResponse;
import com.tom_novak.android.astrumqtest.domain.Message;
import com.tom_novak.android.astrumqtest.domain.Salt;

import java.util.List;

/**
 * Created by tom on 11.11.16.
 */

public interface MainView extends IView, IFormView<String> {

    void showLoginScreen();

    void showMessages(List<Message> messages);
}
