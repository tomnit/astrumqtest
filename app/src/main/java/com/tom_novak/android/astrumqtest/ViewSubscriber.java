package com.tom_novak.android.astrumqtest;

import com.tom_novak.android.astrumqtest.domain.ApiError;
import com.tom_novak.android.astrumqtest.view.IView;

import retrofit2.Retrofit;

/**
 * Created by tom on 14.11.16.
 */

public class ViewSubscriber<T> extends ErrorHandlingSubscriber<T> {

    private final IView view;

    public ViewSubscriber(IView view, Retrofit retrofit) {
        super(retrofit);
        this.view = view;
    }

    @Override
    public void onNext(T t) {
        view.showProgress(false);
    }

    @Override
    protected void showError(ApiError apiError) {
        view.showProgress(false);
        view.showError(apiError);
    }
}
