package com.tom_novak.android.astrumqtest.presenter;

import com.tom_novak.android.astrumqtest.ErrorHandlingSubscriber;
import com.tom_novak.android.astrumqtest.IPreferencesRepository;
import com.tom_novak.android.astrumqtest.api.ApiInterface;
import com.tom_novak.android.astrumqtest.api.HeadersInterceptor;
import com.tom_novak.android.astrumqtest.domain.ApiError;
import com.tom_novak.android.astrumqtest.domain.AuthResponse;
import com.tom_novak.android.astrumqtest.domain.Message;
import com.tom_novak.android.astrumqtest.domain.MessageContainer;
import com.tom_novak.android.astrumqtest.domain.Messages;
import com.tom_novak.android.astrumqtest.view.MainView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by tom on 11.11.16.
 */

public class MainPresenter {

    public static final int MESSAGES_LIMIT = 10;

    private final Retrofit retrofit;
    private final ApiInterface api;
    private final MainView view;
    private final IPreferencesRepository preferences;
    private final HeadersInterceptor interceptor;

    private Subscriber getMessagesSubscriber;
    private Subscriber sendMessageSubscriber;

    private int messagesOffset;
    private boolean hasMore = true;
    private List<Message> messages = new ArrayList<>();

    @Inject
    public MainPresenter(MainView view, Retrofit retrofit,
                         ApiInterface api, IPreferencesRepository preferences,
                         HeadersInterceptor interceptor) {
        this.view = view;
        this.retrofit = retrofit;
        this.api = api;
        this.preferences = preferences;
        this.interceptor = interceptor;
        this.getMessagesSubscriber = new GetMessagesSubscriber(retrofit);
        this.sendMessageSubscriber = new SendMessageSubscriber(retrofit);
    }

    public void onCreate() {
        AuthResponse authData = preferences.getAuthData();
        if(authData != null) {
            interceptor.setAuthToken(authData.getToken());
        } else {
            view.showLoginScreen();
        }
    }

    public boolean isHasMore() {
        return hasMore;
    }

    public void loadNextMessages() {
        if(hasMore) {
            view.showProgress(true);
            doGetMessages(messagesOffset);
        }
    }

    public void doSendMessage() {
        if(!view.isValid()) {
            return;
        }

        String messageDescription = view.getData();
        Message message = new Message();
        message.setTitle("New message");
        message.setDescription(messageDescription);

        Observable<MessageContainer> observable = api.addMessage(message);
        Subscription subscription = observable.observeOn(AndroidSchedulers.mainThread())
                .subscribe(sendMessageSubscriber);

    }

    private void doGetMessages(int messagesOffset) {
        Observable<Messages> observable = api.getMessages(MESSAGES_LIMIT, messagesOffset);
        Subscription subscription = observable.observeOn(AndroidSchedulers.mainThread())
                .subscribe(getMessagesSubscriber);
    }

    private void proceedMessages(Messages messages) {
        hasMore = messages.getHasMore();
        this.messages.addAll(messages.getMessages());
        messagesOffset = this.messages.size();
        view.showMessages(this.messages);
    }

    private void postMessageAdded(Message message) {
        this.messages.add(message);
        view.showMessages(this.messages);
    }

    class GetMessagesSubscriber extends ErrorHandlingSubscriber<Messages> {

        public GetMessagesSubscriber(Retrofit retrofit) {
            super(retrofit);
        }

        @Override
        protected void showError(ApiError apiError) {
            view.showProgress(false);
            view.showError(apiError);
        }

        @Override
        public void onNext(Messages messages) {
            view.showProgress(false);
            proceedMessages(messages);
        }
    }

    class SendMessageSubscriber extends ErrorHandlingSubscriber<MessageContainer> {

        public SendMessageSubscriber(Retrofit retrofit) {
            super(retrofit);
        }

        @Override
        protected void showError(ApiError apiError) {
            view.showProgress(false);
            view.showError(apiError);
        }

        @Override
        public void onNext(MessageContainer container) {
            view.showProgress(false);
            postMessageAdded(container.getMessage());
        }
    }
}
