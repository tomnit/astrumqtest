package com.tom_novak.android.astrumqtest.dagger;

import com.tom_novak.android.astrumqtest.ActivityScope;
import com.tom_novak.android.astrumqtest.activity.LoginActivity;
import com.tom_novak.android.astrumqtest.activity.MainActivity;

import dagger.Component;

/**
 * Created by tom on 13.11.16.
 */

@ActivityScope
@Component(
        modules = LoginModule.class,
        dependencies = AppComponent.class
)
public interface LoginComponent {

    void inject(LoginActivity activity);
}
