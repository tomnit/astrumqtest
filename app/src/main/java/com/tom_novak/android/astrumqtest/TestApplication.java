package com.tom_novak.android.astrumqtest;

import android.app.Application;
import android.content.Context;

import com.tom_novak.android.astrumqtest.dagger.AppComponent;
import com.tom_novak.android.astrumqtest.dagger.AppModule;
import com.tom_novak.android.astrumqtest.dagger.DaggerAppComponent;


/**
 * Created by tom on 11.11.16.
 */

public class TestApplication extends Application {

    private AppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();

        component = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
        component.inject(this);
    }

    public AppComponent getAppComponent() {
        return component;
    }

    public static TestApplication get(Context context) {
        return (TestApplication) context.getApplicationContext();
    }
}
