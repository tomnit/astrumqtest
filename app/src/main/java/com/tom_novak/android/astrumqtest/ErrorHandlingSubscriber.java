package com.tom_novak.android.astrumqtest;

import com.tom_novak.android.astrumqtest.domain.ApiError;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;

/**
 * Created by tom on 14.11.16.
 */

public abstract class ErrorHandlingSubscriber<T> extends Subscriber<T> {

    protected final Retrofit retrofit;

    public ErrorHandlingSubscriber(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    @Override
    public void onCompleted() {
        // TODO
    }

    @Override
    public void onError(Throwable e) {
        if(e instanceof HttpException) {
            HttpException exception = (HttpException) e;
            showError(getResponseError(exception.response()));
            return;
        } else if(e instanceof IOException) {
            showError(null); // TODO send network error type
            return;
        }

        // TODO send unknown error type
        showError(null);
    }

    protected ApiError getResponseError(Response response) {
        ApiError apiError = null;
        try {
            if (response != null && response.errorBody() != null) {
                Converter<ResponseBody, ApiError> converter
                        = retrofit.responseBodyConverter(ApiError.class, new Annotation[0]);
                apiError = converter.convert(response.errorBody());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return apiError;
    }

    protected abstract void showError(ApiError apiError);
}
