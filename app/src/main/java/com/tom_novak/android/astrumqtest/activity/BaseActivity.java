package com.tom_novak.android.astrumqtest.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.tom_novak.android.astrumqtest.R;
import com.tom_novak.android.astrumqtest.TestApplication;
import com.tom_novak.android.astrumqtest.dagger.AppComponent;
import com.tom_novak.android.astrumqtest.domain.ApiError;
import com.tom_novak.android.astrumqtest.view.IView;

import butterknife.BindView;

/**
 * Created by tom on 13.11.16.
 */

public abstract class BaseActivity extends AppCompatActivity implements IView {

    @BindView(R.id.progress_container)
    @Nullable
    View progressContainer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentViewRes());
        setupComponent((AppComponent) TestApplication.get(this).getAppComponent());
    }

    @Override
    public void showProgress(boolean progress) {
        if(progressContainer == null) {
            return;
        }

        if(progress) {
            progressContainer.setVisibility(View.VISIBLE);
        } else {
            progressContainer.setVisibility(View.GONE);
        }
    }

    @Override
    public void showError(ApiError error) {
        String errorMessage = getResources().getString(R.string.unknown_error);
        if(error != null) {
            errorMessage = error.getMessage();
        }
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }

    protected abstract void setupComponent(AppComponent appComponent);

    protected abstract int getContentViewRes();
}
