package com.tom_novak.android.astrumqtest.presenter;

import com.tom_novak.android.astrumqtest.ErrorHandlingSubscriber;
import com.tom_novak.android.astrumqtest.IPreferencesRepository;
import com.tom_novak.android.astrumqtest.api.ApiInterface;
import com.tom_novak.android.astrumqtest.domain.AuthResponse;
import com.tom_novak.android.astrumqtest.domain.ApiError;
import com.tom_novak.android.astrumqtest.domain.UserDetail;
import com.tom_novak.android.astrumqtest.view.RegistrationView;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by tom on 11.11.16.
 */

public class RegistrationPresenter {

    private final Retrofit retrofit;
    private final ApiInterface api;
    private final RegistrationView view;
    private final IPreferencesRepository preferences;

    private Subscription subscription;

    @Inject
    public RegistrationPresenter(RegistrationView view, Retrofit retrofit,
                                 ApiInterface api, IPreferencesRepository preferences) {
        this.view = view;
        this.retrofit = retrofit;
        this.api = api;
        this.preferences = preferences;
    }

    public void doRegister() {
        if(!view.isValid()) {
            return;
        }

        UserDetail data = view.getData();

        Observable<AuthResponse> observable = api.register(data);
        subscription = observable.observeOn(AndroidSchedulers.mainThread())
                .subscribe(new RegistrationSubscriber(retrofit));
    }

    class RegistrationSubscriber extends ErrorHandlingSubscriber<AuthResponse> {

        public RegistrationSubscriber(Retrofit retrofit) {
            super(retrofit);
        }

        @Override
        protected void showError(ApiError apiError) {
            view.showError(apiError);
        }

        @Override
        public void onNext(AuthResponse response) {
            preferences.saveAuthData(response);
            view.processSuccessRegistration(response);
        }
    }

}
